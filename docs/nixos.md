# NixOS installation guide

## Partitioning

### UEFI

Run `gdisk` and partition the drives like so:

| Partition | Hex Code | Size            |
| --------- | -------- | --------------- |
| /dev/sdX1 | ef00     | 300MB           |
| /dev/sdX2 | DEFAULT  | Remaining space |

Then create the filesystem for the `/boot` partition:

```
$ sudo mkfs.fat -F 32 -n boot /dev/sdX1
```

`/dev/sdX2` will be an encrypted LUKS partition containing our `/` and `/home` partitions:

```
$ cryptsetup luksFormat --label crypt /dev/sdX2
$ cryptsetup luksOpen /dev/sdX2 crypt
```

At this point, `/dev/sdX2` will be decrypted and available under `/dev/mapper/crypt`. We can create
our *physical volume* and *volume group* under it:

```
$ pvcreate /dev/mapper/crypt
$ vgcreate vg_crypt /dev/mapper/crypt
```

The last thing to do is to create our *logical volumes* under the volume group we just made and
format them:

```
$ lvcreate -L 50G -n root vg_crypt
$ lvcreate -l 100%FREE -n home vg_crypt
$ mkfs.ext4 -L root /dev/vg_crypt/root
$ mkfs.ext4 -L home /dev/vg_crypt/home
```

When complete, mount the root partition under `/mnt` and create directories for `/boot` and `/home`.

```
$ mount /dev/vg_crypt/root /mnt
$ mkdir /mnt/{boot,home}
$ mount /dev/sdX1 /mnt/boot
$ mount /dev/vg_crypt/home /mnt/home
```

### Legacy Boot

Run `gdisk` and partition the drives like so:

| Partition | Hex Code | Size            |
| --------- | -------- | --------------- |
| N/A       | ef02     | 1MB             |
| /         | DEFAULT  | 50GB            |
| /home     | DEFAULT  | Remaining space |

Then create filesystems for all partitions:

```
$ sudo mkfs.ext4 -L root /dev/[ROOT PARTITION]
$ sudo mkfs.ext4 -L home /dev/[HOME PARTITION]
```

When complete, mount the root partition under `/mnt` and create a directory for `/home`.

```
$ mount /dev/[ROOT PARTITION] /mnt
$ mkdir /mnt/home

$ mount /dev/[HOME PARTITION] /mnt/home
```

## NixOS configuration

### Bare configuration

If you just want a fresh install and don't want to use any pre-existing configuration, run the
following:

```
$ sudo nixos-generate-config --root /mnt
$ sudo nixos-install
```

> **Note**
> 
> `configuration.nix` and `hardware-configuration.nix` files will be generated under `/mnt/etc/nixos`

### Repository configuration

To use my configuration, run:

```
sudo nixos-install --flake https://gitlab.com/agaggi/dotfiles:#<system>
```

> **Note**
> 
> `<system>` should be replaced with one of the names under the `system` directory in the repo (i.e.,
> `dekstop`, `laptop`)

## Final steps

After complete, run the following and reboot:

```
sudo umount -a
reboot
```
