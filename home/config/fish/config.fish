# Auto-login to window manager 
if [ (tty) = "/dev/tty1" ]
    exec river
end

set -x fish_user_paths $HOME/bin $HOME/.cargo/bin $fish_user_paths
set -x TERM xterm-256color
set -x EDITOR nvim
set -x MANPAGER nvim +Man!

# Aliases
alias grep="rg"
alias sudo="doas "
alias vim="nvim"

alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"

alias ls="ls --color=always --group-directories-first"
alias ll="ls --color=always --group-directories-first -Ahlv"
