function fish_right_prompt
    switch $fish_bind_mode
        case insert
            echo -s (set_color --bold green) "INSERT"
        case replace_one
            echo -s (set_color --bold red) "REPLACE"
        case visual
            echo -s (set_color --bold magenta) "VISUAL"
        case "*"
            echo -s (set_color --bold blue) "NORMAL"
    end

    set_color normal
end
