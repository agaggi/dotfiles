function fish_prompt
    echo -s (set_color green) "[" (set_color yellow) $USER \
            (set_color normal) "@" (set_color red) (prompt_hostname) \
            (set_color --bold blue) " " (basename (prompt_pwd)) \
            (set_color normal) (set_color green) "]" (set_color normal) "\$ "
end
